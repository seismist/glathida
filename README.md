# Glacier Thickness Database (GlaThiDa)

This dataset adheres to the Frictionless Data [Tabular Data Package](https://specs.frictionlessdata.io//tabular-data-package) specification. All metadata is provided in `datapackage.yml`. This README is automatically generated from the contents of that file.

- `version` 4.0.0-beta
- `created` 2021-07-21T13:44:35Z
- `id` https://doi.org/10.5904/wgms-glathida-2020-10
- `description` Internationally collected, standardized dataset on glacier thickness from in-situ and remotely sensed observations, based on data submissions, literature review and airborne data from NASA’s Operation IceBridge.

  The latest (development) version of GlaThiDa is available from https://gitlab.com/wgms/glathida. Since the data may contain errors, we strongly suggest performing quality checks and, in case of ambiguities, to contact us as well as the original investigators and agencies. Bug reports, data submissions, and other issues should be posted to the issue tracker at https://gitlab.com/wgms/glathida/-/issues.

  GlaThiDa is a contribution to the International Association of Cryospheric Sciences (IACS) working group on glacier ice thickness estimation (https://cryosphericsciences.org/activities/ice-thickness) chaired by Daniel Farinotti, Liss Marie Andreassen and Huilin Li.
- `publications`
  - [1]
    - `author` Ethan Welty, Michael Zemp, Francisco Navarro, Matthias Huss, Johannes J. Fürst, Isabelle Gärtner-Roer, Johannes Landmann, Horst Machguth, Kathrin Naegeli, Liss M. Andreassen, Daniel Farinotti, Huilin Li, and GlaThiDa Contributors
    - `title` Worldwide version-controlled database of glacier thickness observations
    - `journal` Earth System Science Data
    - `year` 2020
    - `path` https://doi.org/10.5194/essd-2020-87
  - [2]
    - `author` Isabelle Gärtner-Roer, Kathrin Naegeli, Matthias Huss, Thomas Knecht, Horst Machguth, and Michael Zemp
    - `title` A database of worldwide glacier thickness observations
    - `journal` Global and Planetary Change
    - `year` 2014
    - `path` https://doi.org/10.1016/j.gloplacha.2014.09.003
- `homepage` https://www.gtn-g.ch/data_catalogue_glathida
- `profile` tabular-data-package
- `publisher` World Glacier Monitoring Service (WGMS)
- `temporalCoverage` 1935/2018-07-26
- `spatialCoverage` Global
- `languages` ['en']
- `licenses`
  - [1]
    - `title` Creative Commons Attribution 4.0 International
    - `name` CC-BY-4.0
    - `path` https://creativecommons.org/licenses/by/4.0
- `citation` GlaThiDa Consortium (2020): Glacier Thickness Database 3.1.0. World Glacier Monitoring Service, Zurich, Switzerland (https://doi.org/10.5904/wgms-glathida-2020-10)

  To cite a subset of the data, refer to the investigators and references listed in the database. For example – Dowdeswell et al. (2002), in: GlaThiDa Consortium (2020): Glacier Thickness Database 3.1.0. World Glacier Monitoring Service, Zurich, Switzerland (https://doi.org/10.5904/wgms-glathida-2020-10)

  The GlaThiDa Consortium consists of the authors and contributors listed below.

## Credits

### Authors

People who have compiled and maintained GlaThiDa.

  - **Ethan Welty**, University of Colorado: Institute of Arctic and Alpine Research, United States
  - **Francisco J. Navarro**, Universidad Politécnica de Madrid: Escuela Técnica Superior de Ingenieros de Telecomunicación (ETSIT), Spain
  - **Johannes Fürst**, University of Erlangen–Nuremberg (FAU): Institute of Geography, Germany
  - **Isabelle Gärtner-Roer**, University of Zürich, Switzerland
  - **Johannes Landmann**, University of Zürich, Switzerland
  - **Kathrin Naegeli**, University of Fribourg, Switzerland
  - **Matthias Huss**, University of Fribourg: Laboratory of Hydraulics, Hydrology and Glaciology (VAW), Switzerland
  - **Thomas Knecht**, University of Zürich, Switzerland
  - **Horst Machguth**, University of Zürich, Switzerland
  - **Michael Zemp**, University of Zürich, Switzerland

### Contributors

People who have performed measurements, processed data, and/or submitted data to GlaThiDa, listed in alphabetical order by last name. This list does not include the authors of published literature and datasets which were added to GlaThiDa by the authors of GlaThiDa.

  - **Jakob Abermann**, Asiaq Greenland Survey, Greenland
  - **Songtao Ai**, Wuhan University, China
  - **Brian Anderson**, Victoria University of Wellington: Antarctic Research Centre, New Zealand
  - **Liss Marie Andreassen**, Norwegian Water Resources and Energy Directorate (NVE), Norway
  - **Serguei M. Arkhipov**, Russian Academy of Sciences: Institute of Geography (IGRAS), Russia
  - **Izumi Asaji**, Hokkaido University, Japan
  - **Andreas Bauder**, Swiss Federal Institute of Technology (ETH) Zürich: Laboratory of Hydraulics, Hydrology and Glaciology (VAW), Switzerland
  - **Jostein Bakke**, University of Bergen: Department of Earth Sciences, Norway
  - **Toby J. Benham**, Scott Polar Research Institute, United Kingdom
  - **Douglas I. Benn**, University of Saint Andrews, United Kingdom
  - **Daniel Binder**, Central Institute for Meteorology and Geodynamics (ZAMG), Austria
  - **Elisa Bjerre**, Technical University of Denmark: Arctic Technology Centre, Denmark
  - **Helgi Björnsson**, University of Iceland, Iceland
  - **Norbert Blindow**, Institute for Geophysics, University of Münster, Germany
  - **Pascal Bohleber**, Austrian Academy of Sciences (ÖAW): Institute for Interdisciplinary Mountain Research (IGF), Austria
  - **Eliane Brändle**, University of Fribourg, Switzerland
  - **Gino Casassa**, University of Magallanes: GAIA Antarctic Research Center (CIGA), Chile
  - **Jorge Luis Ceballos**, Institute of Hydrology, Meteorology and Environmental Studies (IDEAM), Colombia
  - **Julian A. Dowdeswell**, Scott Polar Research Institute, United Kingdom
  - **Felipe Andres Echeverry Acosta**
  - **Hallgeir Elvehøy**, Norwegian Water Resources and Energy Directorate (NVE), Norway
  - **Rune V. Engeset**, Norwegian Water Resources and Energy Directorate (NVE), Norway
  - **Daniel Farinotti**, Swiss Federal Institute for Forest, Snow and Landscape Research (WSL), Switzerland
  - **Andrea Fischer**, Institute of Interdisciplinary Mountain Research (IGF), Austria
  - **Mauro Fischer**, University of Fribourg, Switzerland
  - **Gwenn E. Flowers**, Simon Fraser University: Department of Earth Sciences, Canada
  - **Erlend Førre**, University of Bergen: Department of Earth Sciences, Norway
  - **Yoshiyuki Fujii**, National Institute of Polar Research, Japan
  - **Johannes Fürst**, University of Erlangen–Nuremberg (FAU): Institute of Geography, Germany
  - **Isabelle Gärtner-Roer**, University of Zürich, Switzerland
  - **Mariusz Grabiec**, University of Silesia in Katowice, Poland
  - **Jon Ove Hagen**, University of Oslo, Norway
  - **Svein-Erik Hamran**, University of Oslo, Norway
  - **Lea Hartl**, Institute of Interdisciplinary Mountain Research (IGF), Austria
  - **Robert Hawley**, Dartmouth College, United States
  - **Kay Helfricht**, Institute of Interdisciplinary Mountain Research (IGF), Austria
  - **Matthias Huss**, University of Fribourg: Laboratory of Hydraulics, Hydrology and Glaciology (VAW), Switzerland
  - **Elisabeth Isaksson**, Norwegian Polar Institute, Norway
  - **Jacek Jania**, University of Silesia in Katowice, Poland
  - **Robert W. Jacobel**, Saint Olaf College: Physics Department, United States
  - **Michael Kennett**, Norwegian Water Resources and Energy Directorate (NVE), Norway
  - **Bjarne Kjøllmoen**, Norwegian Water Resources and Energy Directorate (NVE), Norway
  - **Thomas Knecht**, University of Zürich, Switzerland
  - **Jack Kohler**, Norwegian Polar Institute, Norway
  - **Vladimir Kotlyakov**, Russian Academy of Sciences: Institute of Geography (IGRAS), Russia
  - **Steen Savstrup Kristensen**, Technical University of Denmark: Department of Space Research and Space Technology (DTU Space), Denmark
  - **Stanislav Kutuzov**, University of Reading, United Kingdom
  - **Johannes Landmann**, University of Zürich, Switzerland
  - **Javier Lapazaran**, Universidad Politécnica de Madrid, Spain
  - **Tron Laumann**, Norwegian Water Resources and Energy Directorate (NVE), Norway
  - **Ivan Lavrentiev**, Russian Academy of Sciences: Institute of Geography (IGRAS), Russia
  - **Huilin Li**, Chinese Academy of Sciences: Tianshan Glaciological Station, China
  - **Katrin Lindbäck**, Norwegian Polar Institute, Norway
  - **Peter Lisager**, Asiaq Greenland Survey, Greenland
  - **Horst Machguth**, University of Zürich, Switzerland
  - **Francisco Machío**, Universidad Internacional de La Rioja (UNIR), Spain
  - **Gerhard Markl**, Institute of Interdisciplinary Mountain Research (IGF), Austria
  - **Enrico Mattea**, University of Fribourg: Department of Geography, Switzerland
  - **Kjetil Melvold**, Norwegian Water Resources and Energy Directorate (NVE), Norway
  - **Laurent Mingo**, Blue System Integration Ltd., Canada
  - **Christian Mitterer**, Institute of Interdisciplinary Mountain Research (IGF), Austria
  - **Andri Moll**, University of Zürich, Switzerland
  - **Kathrin Naegeli**, University of Fribourg, Switzerland
  - **Francisco J. Navarro**, Universidad Politécnica de Madrid: Escuela Técnica Superior de Ingenieros de Telecomunicación (ETSIT), Spain
  - **Ian Owens**, University of Canterbury: Department of Geography, New Zealand
  - **Finnur Pálsson**, University of Iceland, Iceland
  - **Rickard Pettersson**, Uppsala University, Sweden
  - **Rainer Prinz**, University of Graz: Department of Geography and Regional Science, Austria
  - **Ya.-M.K. Punning**, Estonian Academy of Sciences (USSR Academy of Sciences-Estonia): Institute of Geology, Estonia
  - **Antoine Rabatel**, University Grenoble Alpes, France
  - **Ian Raphael**, Dartmouth College, United States
  - **David Rippin**, University of York, United Kingdom
  - **Andrés Rivera**, Centro de Estudios Científicos (CECs), Chile
  - **José Luis Rodríguez Lagos**, Centro de Estudios Científicos (CECs), Chile
  - **John Sanders**, University of California, Berkeley: Department of Earth and Planetary Science, United States
  - **Albane Saintenoy**, University of Paris-Sud, France
  - **Arne Chr. Sætrang**, Norwegian Polar Institute, Norway
  - **Marius Schaefer**, Austral University of Chile: Institute of Physical and Mathematical Sciences (ICFM), Chile
  - **Stefan Scheiblauer**, Environmental Earth Observation Information Technology (ENVEO IT GmbH), Austria
  - **Thomas V. Schuler**, University of Oslo, Norway
  - **Heïdi Sevestre**, University of Saint Andrews, United Kingdom
  - **Bernd Seiser**, Institute of Interdisciplinary Mountain Research (IGF), Austria
  - **Ingvild Sørdal**, University of Oslo: Department of Geosciences, Norway
  - **Jakob Steiner**, University of Utrecht: Faculty of Geosciences, Netherlands
  - **Peter Alexander Stentoft**, Technical University of Denmark: Arctic Technology Centre (ARTEK), Denmark
  - **Martin Stocker-Waldhuber**, Technical University of Denmark: Arctic Technology Centre (ARTEK), Denmark
  - **Bernd Seiser**, Institute of Interdisciplinary Mountain Research (IGF), Austria
  - **Shin Sugiyama**, Hokkaido University: Institute of Low Temperature Science, Japan
  - **Rein Vaikmäe**, Tallinn University of Technology, Estonia
  - **Evgeny Vasilenko**, Academy of Sciences of Uzbekistan, Uzbekistan
  - **Nat J. Wilson**, Simon Fraser University: Department of Earth Sciences, Canada
  - **Victor S. Zagorodnov**, Russian Academy of Sciences: Institute of Geography (IGRAS), Russia
  - **Rodrigo Zamora**, Centro de Estudios Científicos (CECs), Chile
  - **Michael Zemp**, University of Zürich, Switzerland

### Sources

Published datasets incorporated into GlaThiDa, listed in order of appearance. This list should not be considered complete.

  - [Ice thickness measurements on South Tyrolean glaciers 1996-2014](https://doi.org/10.1594/PANGAEA.849390)
  - [Ground-penetrating radar (GPR) point measurements of ice thickness in Austria](https://doi.org/10.1594/PANGAEA.849497)
  - [Ice thickness of Kilimanjaro's Northern Ice Field mapped by ground-penetrating radar](https://doi.org/10.1594/PANGAEA.867908)
  - [Swiss Glacier Ice Thickness (release 2018)](https://doi.org/10.18750/icethickness.2018.r2018)
  - [Subglacial topography, ice thickness, and bathymetry of Kongsfjorden, northwestern Svalbard](https://doi.org/10.21334/npolar.2017.702ca4a7)
  - [Pre-IceBridge MCoRDS L2 Ice Thickness, Version 1](https://doi.org/10.5067/QKMTQ02C2U56)
  - [IceBridge HiCARS 1 L2 Geolocated Ice Thickness, Version 1](https://doi.org/10.5067/F5FGUT9F5089)
  - [IceBridge HiCARS 2 L2 Geolocated Ice Thickness, Version 1](https://doi.org/10.5067/9EBR2T0VXUDG)
  - [IceBridge MCoRDS L2 Ice Thickness, Version 1](https://doi.org/10.5067/GDQ0CUCVTE2Q)
  - [IceBridge PARIS L2 Ice Thickness, Version 1](https://doi.org/10.5067/OMEAKG6GIJNB)
  - [IceBridge WISE L2 Ice Thickness and Surface Elevation, Version 1](https://doi.org/10.5067/0ZBRL3GY720R)

## Data structure

The dataset is composed of four tabular data files, referred to here as `survey`, `glacier`, `band`, and `point`.
The metadata describing their structure follows the Frictionless Data [Tabular Data Resource](https://specs.frictionlessdata.io//tabular-data-resource) specification.

All data files share a common format, structure, and encoding:

- `format` csv
- `mediatype` text/csv
- `encoding` utf-8
- `profile` tabular-data-resource
- `dialect`
  - `header` True
  - `delimiter` ,
  - `quoteChar` "
  - `doubleQuote` True
  - `lineTerminator` \n
- `schema`
  - `missingValues` ['']

### `survey` Surveys

- `description` Ice thickness surveys, where each 'survey' represents one or more field campaigns (spanning one or more glaciers, perhaps over multiple years) sharing the same survey method, investigators, agencies, and literature references. All thickness tables (`glacier`, `band`, and `point`) link back to this table.
- `path` data/survey.csv
- `schema`
  - `primaryKey` ['id']

##### `id` Identifier

- `description` Unique survey identifier.
- `type` integer
- `constraints`
  - `required` True
  - `unique` True

##### `from_date` First date

- `description` First date of thickness measurements or, if not known precisely, the earliest possible date (e.g. '2019' -> '2019-01-01').
- `type` date
- `format` %Y-%m-%d

##### `to_date` Last date

- `description` Last date of thickness measurements or, if not known precisely, the latest possible date (e.g. '2019' -> '2019-12-31').
- `type` date
- `format` %Y-%m-%d

##### `platform` Platform

- `description` Survey platform.

  - air: Airborne
  - ground: Terrestrial
  - other: Other (should be described in `method_details`)
- `type` string
- `constraints`
  - `enum` ['air', 'ground', 'other']

##### `method` Method

- `description` Survey method.

  - drill: Drilling (mechanical, hydrothermal, etc)
  - radar: Ground penetrating radar
  - electromagnetic
  - seismic
  - gravimetric
  - other: Should be described in `method_details`
- `type` string
- `constraints`
  - `enum` ['drill', 'radar', 'electromagnetic', 'seismic', 'gravimetric', 'other']

##### `method_details` Method details

- `description` Details useful to assess the accuracy of the ice thickness measurements (e.g. 'GPR full-range system, 100-MHz shielded antenna, constant wave velocity in ice of 0.168 m per ns'). Cannot contain double quotes (") or whitespace other than space.
- `type` string
- `constraints`
  - `pattern` [^"\s]+( [^"\s]+)*

##### `investigators` Investigators

- `description` Names of the people that performed the survey or processed the data, as a pipe-delimited list with identifiers such as affiliation or ORCID (https://orcid.org) in parentheses (e.g. 'Jane Doe (0000-0001-8046-2210) | John Smith | Nanny Murth (University Hill)'). Cannot contain double quotes (") or whitespace other than space.
- `type` string
- `constraints`
  - `pattern` [^"\s]+( [^"\s]+)*

##### `agencies` Agencies

- `description` Names of the agencies that sponsored the survey or hold the data, as a pipe-delimited list with identifiers such as acronym or country in parentheses (e.g. 'Ufficio Idrografico di Bolzano | University of Fribourg (Switzerland)'). Cannot contain double quotes (") or whitespace other than space.
- `type` string
- `constraints`
  - `pattern` [^"\s]+( [^"\s]+)*

##### `references` References

- `description` References to published literature describing the survey, as a pipe-delimited list. Each reference should include at least first author's last name, year, title, and URL in the format `{authors} ({year}): {title} ({url})` (e.g. 'Sanders et al. (2010): Dynamics of an alpine cirque glacier (https://doi.org/10.2475/08.2010.03)'). If a canonical URL (such as a DOI) does not exist, additional information should be provided, in the format `{authors} ({year}): {title}. {journal}, {volume}({issue}), {pages}` (e.g. 'Sanders et al. (2010): Dynamics of an alpine cirque glacier. American Journal of Science, 310(8), 753-773'). Cannot contain double quotes (") or whitespace other than space.
- `type` string
- `constraints`
  - `pattern` [^"\s]+( [^"\s]+)*

##### `remarks` Remarks

- `description` Important information about the overall survey not included elsewhere. Cannot contain double quotes (") or whitespace other than space.
- `type` string
- `constraints`
  - `pattern` [^"\s]+( [^"\s]+)*

### `glacier` Thickness: By glacier

- `description` The name, location, and estimated thickness of surveyed glaciers. This table is required for any thickness estimate of a glacier or glacier elevation band (table `band`), but may optionally be used to assign point measurements (table `point`) to a named glacier.
- `path` data/glacier.csv
- `schema`
  - `primaryKey` ['id']
  - `foreignKeys`
    - [1]
      - `fields` ['survey_id']
      - `reference`
        - `resource` survey
        - `fields` ['id']

##### `id` Identifier

- `description` Unique identifier.
- `type` integer
- `constraints`
  - `required` True
  - `unique` True

##### `survey_id` Survey identifier

- `description` Link to the corresponding survey.
- `type` integer
- `constraints`
  - `required` True

##### `name` Name

- `description` Full name of the glacier (as would appear on a map) in any language or script (e.g. 'Columbia Glacier', 'Übeltalferner', 'Ghiacciaio del Basòdino'). If needed, parentheses are used to specify an unnamed subset of the glacier (e.g. 'Urumqi Glacier No. 1 (east branch)'). Cannot contain double quotes (") or whitespace other than space.
- `type` string
- `constraints`
  - `pattern` [^"\s]+( [^"\s]+)*

##### `external_db` External database

- `description` Database to which `external_id` refers, if provided.

  - GLIMS: Global Land Ice Measurements from Space
  - RGI: Randolph Glacier Inventory
  - WGI: World Glacier Inventory
  - FOG: Fluctuations of Glaciers
  - AGI: Australian Glacier Inventory
  - NVE: Norwegian Water Resources and Energy Directorate Atlas
  - other: Other (full name should be listed in `remarks`)
- `type` string
- `constraints`
  - `enum` ['GLIMS', 'RGI', 'WGI', 'FOG', 'AGI', 'NVE', 'other']

##### `external_id` External identifier

- `description` Identifier of the glacier in `external_db`. For the Randolph Glacier Inventory (RGI), the database version should be included as part of the glacier identifier (e.g. 'RGI60-07.00244'). Cannot contain double quotes (") or whitespace.
- `type` string
- `constraints`
  - `maxLength` 14
  - `pattern` [^"\s]+

##### `lat` Latitude (°, WGS 84)

- `description` Latitude in decimal degrees (°, WGS 84). Positive values indicate the northern hemisphere and negative values indicate the southern hemisphere. Three decimal places may not be sufficient depending on the proximity to other glaciers.

  The point (`lat`, `lon`) should be in the upper part of the glacier ablation area, in the main channel, and sufficiently high so as not to be lost if the glacier retreats.
- `type` number
- `constraints`
  - `required` True
  - `minimum` -90
  - `maximum` 90

##### `lon` Longitude (°, WGS 84)

- `description` Longitude in decimal degrees (°, WGS 84). Positive values indicate east of the zero meridian and negative values indicate west of the zero meridian. Three decimal places may not be sufficient depending on the proximity to other glaciers.

  The point (`lat`, `lon`) should be in the upper part of the glacier ablation area, in the main channel, and sufficiently high so as not to be lost if the glacier retreats.
- `type` number
- `constraints`
  - `required` True
  - `minimum` -180
  - `maximum` 180

##### `date` Date (minimum)

- `description` Date of the glacier thickness (`glacier.*_thickness`) or, if the survey lasted multiple days or the dates are not known precisely, the earliest possible date (e.g. '2019' -> '2019-01-01').
- `type` date
- `format` %Y-%m-%d

##### `max_date` Maximum date

- `description` Latest possible date of the glacier thickness (e.g. '2019' -> '2019-12-31'). Only used if different from `date`.
- `type` date
- `format` %Y-%m-%d

##### `area` Total area (km^2)

- `description` Total glacier area (km^2). If the date for the area is different from `date`–`max_date`, it should be noted in `remarks`.
- `type` number
- `constraints`
  - `minimum` 0

##### `mean_slope` Mean glacier slope (°)

- `description` Mean surface slope over the glacier (°), as an integer. If the date for the slope is different from `date`–`max_date`, it should be noted in `remarks`.
- `type` integer
- `constraints`
  - `minimum` 0
  - `maximum` 90

##### `mean_thickness` Mean glacier thickness (m)

- `description` Mean ice thickness (m), as an integer. Ideally, this represents the interpolated mean over the entire glacier rather than the mean of the individual thickness measurements. Otherwise, it should be noted in `flag` and `remarks`.
- `type` integer
- `constraints`
  - `minimum` 0
  - `maximum` 9999

##### `mean_thickness_uncertainty` Uncertainty of mean glacier thickness (m)

- `description` Estimated random error of `mean_thickness` (m), as an integer.
- `type` integer
- `constraints`
  - `minimum` 0
  - `maximum` 9999

##### `max_thickness` Maximum glacier thickness (m)

- `description` Maximum ice thickness (m), as an integer.
- `type` integer
- `constraints`
  - `minimum` 0
  - `maximum` 9999

##### `max_thickness_uncertainty` Uncertainty of maximum glacier thickness (m)

- `description` Estimated random error of `max_thickness` (m), as an integer.
- `type` integer
- `constraints`
  - `minimum` 0
  - `maximum` 9999

##### `number_points` Number of points

- `description` Number of points used to estimate the thickness of the glacier (`glacier.*_thickness`) or of its elevation bands (`band.*_thickness`).
- `type` integer
- `constraints`
  - `minimum` 0

##### `number_profiles` Number of profiles

- `description` Number of profiles used to estimate the thickness of the glacier (`glacier.*_thickness`) or of its elevation bands (`band.*_thickness`).
- `type` integer
- `constraints`
  - `minimum` 0

##### `length_profiles` Length of profiles (km)

- `description` Length of survey profiles (km) used to estimate the thickness of the glacier (`glacier.*_thickness`) or of its elevation bands (`band.*_thickness`).
- `type` number
- `constraints`
  - `minimum` 0

##### `interpolation_method` Interpolation method

- `description` Interpolation method used to estimate glacier or elevation band ice thickness from survey points.

  - inverse distance: [Inverse distance weighting](https://en.wikipedia.org/wiki/Inverse_distance_weighting) (IDW)
  - triangulation: Triangulation, including [triangulated irregular network](https://en.wikipedia.org/wiki/Triangulated_irregular_network) (TIN)
  - kriging: [Kriging](https://en.wikipedia.org/wiki/Kriging), also known as Gaussian process regression
  - anudem: Drainage-enforcing algorithm [ANUDEM](https://fennerschool.anu.edu.au/research/products/anudem), including ArcInfo TOPOGRID and ArcGIS Topo to Raster
  - spline: [Spline](https://en.wikipedia.org/wiki/Spline_interpolation) functions
  - minimum curvature: Minimum curvature, including with [tension](https://doi.org/10.1190/1.1442837)
  - natural neighbor: [Natural neighbor](https://en.wikipedia.org/wiki/Natural_neighbor_interpolation)
  - mean
  - other: Should be described in `remarks`
- `type` string
- `constraints`
  - `enum` ['inverse distance', 'triangulation', 'kriging', 'anudem', 'spline', 'minimum curvature', 'natural neighbor', 'mean', 'other']

##### `flag` Quality flag

- `description` Whether glacier thickness is suspect or limited to parts of the glacier. All issues should be described in `remarks`.

  - suspect: Suspect glacier thickness
  - partial: Glacier thickness limited to parts of the glacier
  - other: Other issue
- `type` string
- `constraints`
  - `enum` ['suspect', 'partial', 'other']

##### `remarks` Remarks

- `description` Important information about the survey – specific to the glacier – not included elsewhere. Cannot contain double quotes (") or whitespace other than space.
- `type` string
- `constraints`
  - `pattern` [^"\s]+( [^"\s]+)*

### `band` Thickness: By glacier elevation band

- `description` Ice thickness by bands of surface elevation, typically derived from maps of ice thickness.
- `path` data/band.csv
- `schema`
  - `primaryKey` ['glacier_id', 'from_elevation', 'to_elevation']
  - `foreignKeys`
    - [1]
      - `fields` ['glacier_id']
      - `reference`
        - `resource` glacier
        - `fields` ['id']

##### `glacier_id` Glacier survey identifier

- `description` Link to the corresponding glacier survey.
- `type` integer
- `constraints`
  - `required` True

##### `from_elevation` Lower elevation bound (m)

- `description` Lower boundary of the surface elevation band (m), as an integer. Elevations should be relative to mean sea level (geoid) unless noted in `remarks`.
- `type` integer
- `constraints`
  - `required` True
  - `maximum` 9999

##### `to_elevation` Upper elevation bound (m)

- `description` Upper boundary of the surface elevation band (m), as an integer. Elevations should be relative to mean sea level (geoid) unless noted in `remarks`.
- `type` integer
- `constraints`
  - `required` True
  - `maximum` 9999

##### `date` Date (minimum)

- `description` Date of the elevation band thickness (`band.*_thickness`) or, if the survey lasted multiple days or the dates are not known precisely, the earliest possible date (e.g. '2019' -> '2019-01-01').
- `type` date
- `format` %Y-%m-%d

##### `max_date` Maximum date

- `description` Latest possible date of the elevation band thickness survey (e.g. '2019' -> '2019-12-31'). Only used if different from `date`.
- `type` date
- `format` %Y-%m-%d

##### `elevation_date` Elevation date (minimum)

- `description` Date of the surface elevation (`band.*_elevation`) or, if the survey lasted multiple days or the dates are not known precisely, the earliest possible date (e.g. '2019' -> '2019-01-01').
- `type` date
- `format` %Y-%m-%d

##### `max_elevation_date` Maximum elevation date

- `description` Latest possible date of the surface elevation (e.g. '2019' -> '2019-12-31'). Only used if different from `elevation_date`.
- `type` date
- `format` %Y-%m-%d

##### `area` Total area (km^2)

- `description` Total glacier area (km^2) for the elevation band. If the date for the area is different from `date`–`max_date`, it should be noted in `remarks`.
- `type` number
- `constraints`
  - `minimum` 0

##### `mean_slope` Mean slope (°)

- `description` Mean surface slope (°), as an integer, for the elevation band. If the date for the slope is different from `date`–`max_date`, it should be noted in `remarks`.
- `type` integer
- `constraints`
  - `minimum` 0
  - `maximum` 90

##### `mean_thickness` Mean thickness (m)

- `description` Mean ice thickness (m), as an integer, for the elevation band. Ideally, this is the interpolated mean of the entire elevation band rather than the mean of the individual thickness measurements. Otherwise, it should be noted in `flag` and `remarks`.
- `type` integer
- `constraints`
  - `required` True
  - `minimum` 0
  - `maximum` 9999

##### `mean_thickness_uncertainty` Uncertainty of mean thickness (m)

- `description` Estimated random error of `mean_thickness` (m), as an integer.
- `type` integer
- `constraints`
  - `minimum` 0
  - `maximum` 9999

##### `max_thickness` Maximum thickness (m)

- `description` Maximum ice thickness (m), as an integer, for the elevation band.
- `type` integer
- `constraints`
  - `minimum` 0
  - `maximum` 9999

##### `max_thickness_uncertainty` Uncertainty of maximum thickness (m)

- `description` Estimated random error of `max_thickness` (m), as an integer.
- `type` integer
- `constraints`
  - `minimum` 0
  - `maximum` 9999

##### `flag` Quality flag

- `description` Whether ice thickness is suspect or limited to parts of the elevation band. Issues specific to the elevation band should be described in `remarks` while issues common to all elevation bands should be described in `glacier.remarks`.

  - suspect: Suspect elevation band thickness
  - partial: Elevation band thickness limited to parts of the elevation band
  - other: Other issue
- `type` string
- `constraints`
  - `enum` ['suspect', 'partial', 'other']

##### `remarks` Remarks

- `description` Important information about the survey – specific to the elevation band – not included elsewhere. Cannot contain double quotes (") or whitespace other than space.
- `type` string
- `constraints`
  - `pattern` [^"\s]+( [^"\s]+)*

### `point` Thickness: By point

- `description` Glacier thickness measured at specific points.
- `path` data/point.csv
- `schema`
  - `foreignKeys`
    - [1]
      - `fields` ['survey_id']
      - `reference`
        - `resource` survey
        - `fields` ['id']
    - [2]
      - `fields` ['glacier_id']
      - `reference`
        - `resource` glacier
        - `fields` ['id']

##### `survey_id` Survey identifier

- `description` Link to the corresponding survey.
- `type` integer
- `constraints`
  - `required` True

##### `glacier_id` Glacier identifier

- `description` Link to the corresponding glacier survey, if applicable.
- `type` integer

##### `profile_id` Profile identifier

- `description` Identifier for the survey profile to which the point belongs (if applicable). Should serve to distinguish between different profiles and sort the profiles in the order in which they were collected (e.g. 1, 2, 3, ...).
- `type` string
- `constraints`
  - `maxLength` 8

##### `point_id` Point identifier

- `description` Identifier for the point. Should serve to link back to the original data and/or to sort the points in the order in which they were collected (e.g. 1, 2, 3, ...).
- `type` string
- `constraints`
  - `required` True
  - `maxLength` 8

##### `date` Date (minimum)

- `description` Date of the point thickness (`thickness`) or, if the date is not known precisely, the earliest possible date (e.g. '2019' -> '2019-01-01').
- `type` date
- `format` %Y-%m-%d

##### `max_date` Maximum date

- `description` Latest possible date of the point thickness (e.g. '2019' -> '2019-12-31'). Only used if different from `date`.
- `type` date
- `format` %Y-%m-%d

##### `elevation_date` Elevation date (minimum)

- `description` Date of the surface elevation (`elevation`) or, if the date is not known precisely, the earliest possible date (e.g. '2019' -> '2019-01-01').
- `type` date
- `format` %Y-%m-%d

##### `max_elevation_date` Maximum elevation date

- `description` Latest possible date of the surface elevation (e.g. '2019' -> '2019-12-31'). Only used if different from `elevation_date`.
- `type` date
- `format` %Y-%m-%d

##### `lat` Latitude (°, WGS 84)

- `description` Latitude in decimal degrees (°, WGS 84). Positive values indicate the northern hemisphere and negative values indicate the southern hemisphere.
- `type` number
- `constraints`
  - `required` True
  - `minimum` -90
  - `maximum` 90

##### `lon` Longitude (°, WGS 84)

- `description` Longitude in decimal degrees (°, WGS 84). Positive values indicate east of the zero meridian and negative values indicate west of the zero meridian.
- `type` number
- `constraints`
  - `required` True
  - `minimum` -180
  - `maximum` 180

##### `elevation` Elevation (m)

- `description` Point elevation (m), as an integer. Elevations should be relative to mean sea level (geoid) unless noted in `remarks`.
- `type` integer
- `constraints`
  - `maximum` 999999

##### `thickness` Ice thickness (m)

- `description` Ice thickness (m), as an integer, measured at the point.
- `type` integer
- `constraints`
  - `required` True
  - `minimum` 0
  - `maximum` 9999

##### `thickness_uncertainty` Uncertainty of ice thickness (m)

- `description` Estimated random error of `thickness` (m), as an integer.
- `type` integer
- `constraints`
  - `minimum` 0
  - `maximum` 9999

##### `flag` Quality flag

- `description` Whether ice thickness is suspect. Issues specific to the point should be described in `remarks` while issues common to all points should be described in `survey.remarks` (or `glacier.remarks`, if applicable).

  - suspect: Suspect ice thickness
  - other: Other issue
- `type` string
- `constraints`
  - `enum` ['suspect', 'other']

##### `remarks` Remarks

- `description` Important information about the survey – specific to the point – not included elsewhere. Cannot contain double quotes (") or whitespace other than space.
- `type` string
- `constraints`
  - `pattern` [^"\s]+( [^"\s]+)*

