import dbc.util

package = dbc.util.read_yaml('datapackage.yml')
txt = dbc.util.render_template(
  'scripts/readme.md.jinja', {'package': package}
)
with open('README.md', 'w') as file:
  file.write(txt)
