#### Data description


#### Submission checklist
<!-- Replace [ ] with [x] for checks you have performed. -->

- [ ] Data are provided as either:
  - a link
  - attached to this issue
- [ ] Data are structured as either:
  - 2 to 4 CSV files – `survey.csv`, `glacier.csv`, `point.csv`, `band.csv`
  - 1 Excel file or linked Google Sheet with 2 to 4 sheets - `survey`, `glacier`, `point`, `band`
- [ ] Each table contains only:
  - Column names in the first row (header)
  - Column values in the proceeding rows
- [ ] Column names and values match the "Data structure" described in the [README](https://gitlab.com/wgms/glathida/-/blob/main/README.md#data-structure)
