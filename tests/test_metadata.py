from .setup import package


def test_datapackage_metadata():
    for error in package.metadata_errors:
        print(error)
    assert not package.metadata_errors
