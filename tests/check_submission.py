#!/usr/bin/env python
from pathlib import Path
import sys
from typing import Any, Dict, Iterable, Union
import warnings

import pandas as pd
from validator.schema import Report

from .schemas import BEFORE_FRICTIONLESS, FRICTIONLESS, AFTER_FRICTIONLESS


def read_csvs(paths: Iterable[Union[str, Path]]) -> Dict[str, pd.DataFrame]:
  return {path.stem: pd.read_csv(path, dtype='string') for path in paths}


def write_csvs(
  dfs: Dict[str, pd.DataFrame],
  path: Union[str, Path],
  index: bool = False,
  **kwargs: Any
) -> None:
  for key, df in dfs.items():
    df.to_csv(Path(path) / f'{key}.csv', index=index, **kwargs)


def read_excel(path: Union[str, Path]) -> Dict[str, pd.DataFrame]:
  book = pd.ExcelFile(path)
  dfs = {}
  for name in book.sheet_names:
    with warnings.catch_warnings():
      # Suppress openpyxl warnings that validation and formatting are ignored
      warnings.simplefilter('ignore')
      df = book.parse(sheet_name=name, dtype='string')
    dfs[name] = df
  return dfs


def print_report(report: Report) -> None:
  if report.valid:
    print('Valid data [true]')
    print(report)
  else:
    print('Valid data [false]')
    print(report)
    df = report.to_dataframe()
    # errors
    is_error = df['code'] == 'error'
    if is_error.any():
      print('[ERRORS]')
      print(df[is_error][['table', 'column', 'row', 'check']])
    # failures
    is_fail = df['code'] == 'fail'
    if is_fail.any():
      print('[FAILURES]')
      print(df[is_fail][['table', 'column', 'row', 'check']])


def load(
  path: Union[str, Path],
  to_csvs: bool = False
) -> Dict[str, pd.DataFrame]:
  path = Path(path)
  files = [x for x in path.iterdir() if x.is_file()]
  excel_files = [x for x in files if x.suffix.lower() in ('.xls', '.xlsx')]
  csv_files = [x for x in files if x.suffix.lower() == '.csv']
  if excel_files:
    if len(excel_files) > 1:
      raise ValueError(f'Multiple Excel files found: {excel_files}')
    dfs = read_excel(excel_files[0])
    if to_csvs:
      write_csvs(dfs, path)
      return load(path)
    return dfs
  if csv_files:
    return read_csvs(csv_files)
  raise ValueError(f'No suitable files found among {files}')


def validate(dfs: Dict[str, pd.DataFrame]) -> Report:
  validation = BEFORE_FRICTIONLESS + FRICTIONLESS + AFTER_FRICTIONLESS
  return validation(dfs)


if __name__ == '__main__':
  path = Path(sys.argv[1])
  dfs = load(path)
  report = validate(dfs)
  print_report(report)
  assert report.valid
