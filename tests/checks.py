from typing import Any, Dict, Hashable, Sequence

import pandas as pd
import numpy as np

from validator import Column, register_check


@register_check(
  message='{columns} are not either all null/missing or all not null'
)
def columns_all_null_or_all_not_null(
  df: pd.DataFrame, *, columns: Sequence[Hashable]
) -> pd.Series:
  present = [column for column in columns if column in df]
  sums = df[present].notnull().sum(axis='columns')
  if len(present) == len(columns):
    return sums.eq(0) | sums.eq(len(columns))
  return sums.eq(0)


@register_check(
  message='Value is not null although {column} is null/missing'
)
def null_if_column_null(
  s: pd.Series, df: pd.DataFrame, *, column: Hashable
) -> pd.Series:
  if column not in df:
    return s.isnull()
  return df[column].notnull() | (df[column].isnull() & s.isnull())


@register_check(
  message='Column not null although {columns} are all null/missing',
  required=lambda columns: [Column(column) for column in columns]
)
def null_if_columns_null(
  s: pd.Series, df: pd.DataFrame, *, columns: Sequence[Hashable]
) -> pd.Series:
  present = [column for column in columns if column in df]
  all_null = df[present].notnull().sum(axis='columns').eq(0)
  return ~all_null | (all_null & s.isnull())


@register_check(
  message='Column not null although no interpolated thickness',
)
def null_if_no_interpolated_thickness(
  s: pd.Series, df: pd.DataFrame, dfs: Dict[Hashable, pd.DataFrame]
) -> pd.Series:
  interpolated = pd.Series(False, index=s.index, dtype='boolean')
  if 'mean_thickness' in df:
    interpolated[df['mean_thickness'].notnull()] = True
  if 'max_thickness' in df:
    interpolated[df['max_thickness'].notnull()] = True
  if 'id' in df and 'band' in dfs and 'glacier_id' in dfs['band']:
    interpolated[df['id'].isin(dfs['band']['glacier_id'])] = True
  return interpolated | s.isnull()


@register_check(
  message='Value is null although {column} is {value}',
  required=lambda column: [Column(column)]
)
def not_null_if_column_equal_to(
  s: pd.Series, df: pd.DataFrame, *, column: Hashable, value: Any
) -> pd.Series:
  return df[column].ne(value) | (df[column].eq(value) & s.notnull())


@register_check(message='Survey has no glaciers or points')
def survey_has_glacier_or_point(
  s: pd.Series, dfs: Dict[Hashable, pd.DataFrame]
) -> pd.Series:
  valid = pd.Series(False, dtype='boolean', index=s.index)
  for table in ('glacier', 'point'):
    if table in dfs and 'survey_id' in dfs[table]:
      valid |= s.isin(dfs[table]['survey_id'])
  return valid


@register_check(message='Glacier has no glacier, band, or point thickness')
def glacier_has_thickness(
  s: pd.Series, df: pd.DataFrame, dfs: Dict[Hashable, pd.DataFrame]
) -> pd.Series:
  valid = pd.Series(False, dtype='boolean', index=s.index)
  for column in ('mean_thickness', 'max_thickness'):
    if column in df:
      valid |= df[column].notnull()
  for table in ('band', 'point'):
    if table in dfs and 'glacier_id' in dfs[table]:
      valid |= s.isin(dfs[table]['glacier_id'])
  return valid


@register_check(
  message='Does not match {regex} although {column} is equal to {value}',
  required=lambda column: [Column(column)]
)
def matches_regex_when_column_equal_to(
  s: pd.Series, df: pd.DataFrame, *, column: Hashable, value: Any, regex: str
) -> pd.Series:
  filter = df[column].eq(value)
  return s[filter].str.match(regex)


@register_check(
  message='Point (lat, lon) is not within {distance} km of glacier (lat, lon)',
  required=[
    Column('glacier_id'), Column('lat'), Column('lon'),
    Column('id', 'glacier'), Column('lat', 'glacier'), Column('lon', 'glacier')
  ]
)
def point_near_glacier(
  df: pd.DataFrame, dfs: Dict[Hashable, pd.DataFrame], *, distance: float
) -> pd.Series:
  mask = df['glacier_id'].notnull()
  merge = df.loc[mask, ['glacier_id', 'lat', 'lon']].merge(
    dfs['glacier'][['id', 'lat', 'lon']],
    left_on='glacier_id',
    right_on='id',
    how='left',
    validate='many_to_one',
    suffixes=(None, '.glacier')
  )
  merge.index = df.index[mask]
  point_lat_rad = np.radians(merge['lat'])
  lat_rad = np.radians(merge['lat.glacier'])
  # Use great circle distance (km)
  distances = 6371 * np.arccos(
    np.sin(point_lat_rad) * np.sin(lat_rad) +
    np.cos(point_lat_rad) * np.cos(lat_rad) *
    np.cos(np.radians(merge['lon'] - merge['lon.glacier']))
  )
  return distances < distance
