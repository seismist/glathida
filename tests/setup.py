from pathlib import Path
import frictionless
import pandas as pd


package = frictionless.Package('datapackage.yml')
if not package.metadata_errors:
  from .schemas import BEFORE_FRICTIONLESS, FRICTIONLESS, AFTER_FRICTIONLESS
  validation = BEFORE_FRICTIONLESS + FRICTIONLESS + AFTER_FRICTIONLESS
  paths = Path('data').rglob('*.csv')
  dfs = {path.stem: pd.read_csv(path, low_memory=False) for path in paths}
  report = validation(dfs)
