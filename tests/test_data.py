import pytest

from .setup import report, package


if package.metadata_errors:
  pytest.skip("Skipping data tests", allow_module_level=True)


def test_data():
  if report.valid:
    print('Valid data [true]')
    print(report)
  else:
    print('Valid data [false]')
    print(report)
    df = report.to_dataframe()
    # errors
    is_error = df['code'] == 'error'
    if is_error.any():
      print('[ERRORS]')
      print(df[is_error][['table', 'column', 'row', 'check']])
    # failures
    is_fail = df['code'] == 'fail'
    if is_fail.any():
      print('[ERRORS]')
      print(df[is_error][['table', 'column', 'row', 'check']])
  assert report.valid
