from validator import Check, Column, Schema, Table, Tables
import validator.convert
import yaml

from . import checks


BEFORE_FRICTIONLESS = Schema({
  Tables(): [
    Check(
      lambda dfs: 'survey' in dfs,
      message="Missing 'survey' table (always required)"
    ),
    Check(
      lambda dfs: 'glacier' in dfs or 'point' in dfs,
      message="Missing 'glacier' and 'point' tables (at least one is required)"
    ),
    Check(
      lambda dfs: 'glacier' in dfs if 'band' in dfs else True,
      message="Missing 'glacier' table (required when 'band' is provided)"
    )
  ]
})


FRICTIONLESS = validator.convert.frictionless.package_to_schema(
  yaml.safe_load(open('datapackage.yml'))
)


AFTER_FRICTIONLESS = Schema({
  # Columns either all null or all not null
  Table('glacier'): Check.columns_all_null_or_all_not_null(
    ['external_db', 'external_id']
  ),
  # Null if other column is null
  Column('mean_thickness_uncertainty', table='glacier'):
  Check.null_if_column_null('mean_thickness'),
  Column('max_thickness_uncertainty', table='glacier'):
  Check.null_if_column_null('max_thickness'),
  Column('mean_thickness_uncertainty', table='band'):
  Check.null_if_column_null('mean_thickness'),
  Column('max_thickness_uncertainty', table='band'):
  Check.null_if_column_null('max_thickness'),
  Column('thickness_uncertainty', table='point'):
  Check.null_if_column_null('thickness'),
  # Null if other columns are all null
  Column('flag', table='glacier'): Check.null_if_columns_null(
    ['mean_thickness', 'max_thickness']
  ),
  Column('flag', table='band'): Check.null_if_columns_null(
    ['mean_thickness', 'max_thickness']
  ),
  # Null if no interpolated thickness
  Column('interpolation_method', 'glacier'):
  Check.null_if_no_interpolated_thickness(),
  # Not null if column equal to
  Column('method_details', table='survey'):
  Check.not_null_if_column_equal_to('method', 'other'),
  Column('method_details', table='survey'):
  Check.not_null_if_column_equal_to('platform', 'other'),
  Column('remarks', table='glacier'):
  Check.not_null_if_column_equal_to('external_db', 'other'),
  Column('remarks', table='glacier'):
  Check.not_null_if_column_equal_to('interpolation_method', 'other'),
  Column('id', 'survey'): Check.survey_has_glacier_or_point(),
  Column('id', 'glacier'): Check.glacier_has_thickness(),
  Column('external_id', 'glacier'): Check.matches_regex_when_column_equal_to(
    'external_db', 'GLIMS', r'^G[0-9]{6}E[0-9]{5}N$'
  ),
  Column('external_id', 'glacier'): Check.matches_regex_when_column_equal_to(
    'external_db', 'RGI', r'^RGI(20|32|40|50|60)\-[0-1][0-9]\.[0-9]{5}$'
  ),
  Column('external_id', 'glacier'): Check.matches_regex_when_column_equal_to(
    'external_db', 'WGI', r'^[A-Z]{2}[0-9][A-Z][0-9A-Z]{8}$'
  ),
  Column('external_id', 'glacier'): Check.matches_regex_when_column_equal_to(
    'external_db', 'FOG', r'^[0-9]{,4}$'
  ),
  Table('point'): Check.point_near_glacier(30)
})
